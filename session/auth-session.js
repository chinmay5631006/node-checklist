const express = require('express');
const session = require('express-session');
const mongoose = require('mongoose');
const mongoDBSession = require('connect-mongodb-session')(session);
const app = express();
const session_age = 1000*60*60*2;
const bcrypt = require('bcrypt');


const mongoURI = 'mongodb://127.0.0.1:27017/session'
mongoose.connect(mongoURI,{
     useNewUrlParser:true,
})
    .then(res => {console.log('MongoDB connected')})
    .catch(err => {console.log('failed to connect',err)})

const schema = new mongoose.Schema({
    email: {
        type:String,
        required:true
    },
    password: {
        type:String,
        required:true
    }
});
let userData =  mongoose.model('Data', schema);

const store = new mongoDBSession({
    uri: mongoURI,
    collection: "mysessions" ,
});

app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.use(
    session({
        secret: 'key that will sign cookies',
        resave: false,
        saveUninitialized: false, 
        store: store,
        cookie: {
            secure:false,
            maxAge:session_age,
            sameSite:true,
        },
    })
);

app.get('/', (req,res)=>{
    
    if(req.session.page_count){

        req.session.page_count++;
        console.log(req.session);
        
        res.send(`you visited this page on ${req.session.page_count} times`);
//sid = s%3AvvzY8_iIaYSQuc63m8fie9VB0kAGN-a3.dgUReuKjlRIiP6WAgOI6SdsegH4qDFSuO4QsSBEfIlw for n times
    }else{

        req.session.page_count = 1;
        console.log(req.session);
        res.send("welcomefor 1st time")
    }
//sid = s%3AvvzY8_iIaYSQuc63m8fie9VB0kAGN-a3.dgUReuKjlRIiP6WAgOI6SdsegH4qDFSuO4QsSBEfIlw for 1st time
});

app.get('/register', (req,res)=>{
    return res.send(`
        <form method="post" action="/register" >
            <input type="email" name="email" placeholder="Email" required/>
            <input type="password" name="password" placeholder="Password" required/>         
            <button type="submit" >Register</button>
        </form>
    `)
})

app.get('/login', (req,res)=>{
    return res.send(`
        <form method="post" action="/login">
            <input type="email" name="email" placeholder="Email" required />
            <input type="password" name="password" placeholder="Password" require />         
            <button type="submit" >Login</button>
        </form>
    `)
})

app.get('/home', (req,res)=>{
    res.send("...");
})
// app.get('/register' (req,res)=>{})

app.post('/register', async (req, res, next)=>{
    //console.log(req.heade)
    try{
        let body = req.body;
        console.log(body)
        let ifExists = await userData.find({email:body.email}) ;
        console.log(ifExists);
        if(ifExists.length === 0){
                const salt = await bcrypt.genSalt(10);
                const hashPassword = await bcrypt.hash(body.password, salt)
                const Email = body.email;
                const Password = hashPassword;
                udetails = {email:body.email, password:body.password, hash:Password};
                console.log(udetails);
                let user = new userData({
                    email:Email,
                    password:Password 
                });
                user.save((err,data)=>{
                    if(err){
                        console.log(err);
                        res.send(err.message);
                    }else{
                        //console.log(data);
                        //res.send(data);
                        req.session.userid=user.id;
                        return res.redirect("/login");
                        //console.log("hey",req.session.userid);
                    }
                })
                
            }else{
                 res.redirect('/login');
            }
    }catch(e){
        console.log(e);
    }
     
})
app.post('/login', async (req, res)=>{
    const email = req.body.email;
    const pass = req.body.password;
    // let ifExists = await userData.find({email:req.body.email});
    // if(ifExists.length === 0){
    //     window.alert("No user found, Please REGISTER")
    // }else{
    //     req.session.userid = user.id
    //     console.log(user.id);
    //     res.redirect("/home");
    // }

    if(email && pass){
        let user = await userData.findOne({email:email, password:pass});
        console.log(user);

        if(user){
            req.session.id = user.id;
            res.redirect("/home");
        }else{
            res.redirect('/login');
        }
    }
})

//app.get('/register' (req,res)=>{})
app.listen(5050);

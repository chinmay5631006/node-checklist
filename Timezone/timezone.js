// let time_date = require('date-fns-tz');

// let d = new Date();
// //console.log(d);
// let local_time = d.getTime();
// //console.log(local_time);
// localoffset = d.getTimezoneOffset()*60000;
// //console.log(localoffset);
// let offset = 5.5;
// const utc = local_time+localoffset;
// let bombay = utc + (3600000*offset);

// date = new Date(bombay);
//console.log(date);
const moment = require('moment-timezone');
const express = require('express')
const app = express();
const PORT= 4000;
const timezones = [
    {tz: moment().tz('India/Mumbai').format() , name:'Bombay'},
    {tz: moment().tz('India/Chennai').format(), name:'Chennai'},
    {tz: moment().tz('India/Gujrat').format(), name:'Gujrat'},
    {tz: moment().tz('India/West Bengal').format(), name:'West Bengal'},
    {tz: moment().tz('India/Bihar').format(), name:'Bihar'}
]

app.get('/api/time', (req,res)=>{
    res.send(timezones);
});

app.get('/api/time/:name', (req,res)=>{
    const city = timezones.find(c => c.name===req.params.name)
    //console.log(city)
    if(!city) return res.status(404).send('give valid city');
    return res.send(city);
})


app.listen(PORT, ()=>console.log(`Server listening on ${PORT}`))
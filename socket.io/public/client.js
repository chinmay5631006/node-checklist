const socket = io(); /// io() is comming from client library :---- socket.io/socket.io.js
const textArea = document.querySelector('#textarea')
const messageArea = document.querySelector('.message_area')

let username;

do {
    username = prompt("Enter your Name")
}while(!username)

textArea.addEventListener('keyup', (e) => {
    if(e.key === 'Enter'){
        sendMessage(e.target.value)
    }
})

//function

function sendMessage(message){
    let msg = {
        user: username,
        message: message.trim()
    }

    //append

    appendMessage(msg, 'outgoing')
    textArea.value = '';
    scrollToBottom()

    // send to server via socket connection

    socket.emit('message', msg)
}

function appendMessage(msg, type){
    let mainDiv = document.createElement('div')
    let className = type
    mainDiv.classList.add(className, 'message');

    //generate markup

    let markup = `
        <h4>${msg.user}</h4>
        <p>${msg.message}</p>
    `

    mainDiv.innerHTML = markup;
    messageArea.appendChild(mainDiv);
}

// Receive msg

socket.on('message', (msg) => {
    appendMessage(msg, 'incoming')
    scrollToBottom();
})

//scroll to bottom

function scrollToBottom() {
    messageArea.scrollTop = messageArea.scrollHeight;
}

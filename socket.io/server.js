const express = require('express');
const { Socket } = require('socket.io');
const app = express();
const http = require('http').createServer(app);
const port = 3000;

http.listen(port, () => {
    console.log(`server running on port ${port}`);
})

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

/// SOCKET

const io = require('socket.io')(http);

io.on('connection', (socket) => {
    console.log("connected.....")

    //listening the emited meessage (from sendmesage func)
    socket.on('message', (msg) =>{
        socket.broadcast.emit('message',msg);
    })
})
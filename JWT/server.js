require('dotenv').config();

const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

app.use(express.json());

let count = 0;
const posts = [];
let cloneArray = [];


app.get('/posts',[authorizationToken],(req,res)=>{

        //Authorization
        
        try {
            //console.log('hi');
            const post = posts.filter(post => post.username === req.user.username)
            console.log('matched and verified')
            res.json(post);
        } catch (e) {
            console.log(e)
            res.send('failed')
        }
    });
    
app.post('/register', async (req,res)=>{
    const password = req.body.password;
    if(!posts.find(post => post.password === req.body.password)){

        //Authenticaton Logic

            const salt = await bcrypt.genSalt(10);
            const hashPassword = await bcrypt.hash(req.body.password, salt)
            const newUser = {username:req.body.username, password:req.body.password};
            const Username = newUser.username;
            const Userpassword = hashPassword;
            posts.push(newUser);
            res.send({Username,Userpassword});
            // console.log(posts);
    }else{
        res.send('user already existed');
        count++;
    }
    cloneArray = JSON.parse(JSON.stringify(posts));
    //console.log("cloneArray", cloneArray)
   //console.log("posts",posts)
})

app.post('/login', (req,res)=>{
    // // const salt = await bcrypt.genSalt(10);
    // // const hashPassword = await bcrypt.hash(req.body.password, salt)
    // const user = { username: req.body.username, password: req.body.password};
    // console.log(user);
    // ;
    // posts.push(user);
    let status = cloneArray.find(record => record.username === req.body.username);
    //console.log(status);
    if(status===undefined){
        res.send("invalid username");
        console.log("invalid username");
    }else{
        if(status.password === req.body.password){
            const accessToken = jwt.sign(status, process.env.ACCESS_SECRET_TOKEN);
            //console.log("hi");
            res.json({accessToken:accessToken});
        }
    }
})

//  async function authenticateUser(req,res,next){
//     const salt = await bcrypt.genSalt(10);
//     const hashPassword = await bcrypt.hash(req.body.password, salt)
//     console.log(salt, hashPassword);
//     const verifyUser = {username: req.body.username, password: hashPassword};
//     res.send(verifyUser);
//     next();
// }

function authorizationToken(req, res, next){    
    //console.log(req.headers['authorization']);
    const authHeader = req.headers['authorization'];
    //console.log(authHeader.startsWith('Bearer '))
    if (!authHeader.startsWith('Bearer ')) {
        return res.sendStatus(401);
    }
    const token = authHeader && authHeader.split(' ')[1];
    //console.log('hjjjj');
    if(token == null) return res.status(401);
    
    jwt.verify(token, process.env.ACCESS_SECRET_TOKEN, (err, user)=>{ 
        if(err) return res.status(401);
        console.log(user);
        req.user= user;
        next();
    })
}

app.listen(3000);
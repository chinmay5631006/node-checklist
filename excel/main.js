const { Worker, isMainThread, parentPort } = require("worker_threads");
const json2xls = require("json2xls");
const fs = require("fs/promises");

async function worker() {
    for (let i = 3; i < 31; i++) {
        const data = await fs.readFile(`./json/test_data${i}.json`, "utf8");
        const jsonData = await JSON.parse(data);
        const xls = await json2xls(jsonData);
        await fs.writeFile(`./excel/xls-${i}.xlsx`, xls, "binary");
        console.log(`Generating file xls-${i}.xlsx`);
    }
    parentPort.postMessage("Worker thread finished converting files.");
}
module.exports = worker;

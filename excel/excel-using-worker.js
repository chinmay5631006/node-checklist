const { Worker, isMainThread, parentPort } = require("worker_threads");
const json2xls = require("json2xls");
const fs = require("fs/promises");
const worker = require('./excel1');
const express = require('express');
const app = express();

app.get('/test', (req, res) => {
    var count;
    for(let i=0; i<=1000000; i++) {
        count = i;
    }
    console.log(count);
    if(count) {
        res.status(200).send(count)
    }else{
        res.status(400)
    }
})

app.get('/excel', (req, res) => {
    console.time("creating");

async function tel_lene_wala_function() {
  if (isMainThread) {

    for (let i = 1; i <= 2; i++) {
      const data = await fs.readFile(`./json/test_data${i}.json`, "utf8");
      const jsonData = await JSON.parse(data);
      const xls = await json2xls(jsonData);
      await fs.writeFile(`./excel/xls${i}.xlsx`, xls, "binary");
      console.log(`Generating file xls-${i}.xlsx`);
    }

    console.timeEnd("creating");

  } else {
    console.time("Generating");
    const worker = new Worker("./excel1.js");
    worker.on("message", (message) => {
      console.log('message');
    });
    console.log('hi');
    worker();
    console.log('hey');
    // for (let i = 3; i < 31; i++) {
    //   const data = await fs.readFile(`./json/test_data${i}.json`, "utf8");
    //   const jsonData = await JSON.parse(data);
    //   const xls = await json2xls(jsonData);
    //   await fs.writeFile(`./excel/xls-${i}.xlsx`, xls, "binary");
    //   console.log(`Generating file xls-${i}.xlsx`);
    // }
    // parentPort.postMessage("Worker thread finished converting files.");
  }
  console.timeEnd("Generating");
}
tel_lene_wala_function();

})

// console.time("creating");

// async function tel_lene_wala_function() {
//   if (isMainThread) {

//     for (let i = 1; i <= 2; i++) {
//       const data = await fs.readFile(`./json/test_data${i}.json`, "utf8");
//       const jsonData = await JSON.parse(data);
//       const xls = await json2xls(jsonData);
//       await fs.writeFile(`./excel/xls${i}.xlsx`, xls, "binary");
//       console.log(`Generating file xls-${i}.xlsx`);
//     }

//     console.timeEnd("creating");

//   } else {
//     console.time("Generating");
//     const worker = new Worker("./excel1.js");
//     worker.on("message", (message) => {
//       console.log('message');
//     });
//     console.log('hi');
//     worker();
//     console.log('hey');
//     // for (let i = 3; i < 31; i++) {
//     //   const data = await fs.readFile(`./json/test_data${i}.json`, "utf8");
//     //   const jsonData = await JSON.parse(data);
//     //   const xls = await json2xls(jsonData);
//     //   await fs.writeFile(`./excel/xls-${i}.xlsx`, xls, "binary");
//     //   console.log(`Generating file xls-${i}.xlsx`);
//     // }
//     // parentPort.postMessage("Worker thread finished converting files.");
//   }
//   console.timeEnd("Generating");
// }
// tel_lene_wala_function();

app.listen(3000)

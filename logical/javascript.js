function secondGreatest(){
    arr = [12, 35, 1, 10, 34, 1, 35];

    let first = -Infinity
    let second = -Infinity
    //console.log(second);

    for(let i=0; i<arr.length; i++){
        if(arr[i]>first){
            second = first;
            first = arr[i];
        }
        else if(arr[i]<first && arr[i]>second){
            second = arr[i];
        }
    }
    console.log(second);
}
// secondGreatest();


function reverseString(){
    let string = 'soo goood!!!!!';
    let a = string.length - 1;
    while(a>=0){
        console.log(string.charAt(a))
        a--;
    }
}
// reverseString();


function swap(){
    let a =10;
    let b=12;
    
    a=a*b;
    b=a/b;
    console.log('b',b);
    a=a/b;
    console.log('a',a);
}
// swap()


function revInt(){
    let a =1234;
    let temp = 0;
    while(a>0){
        let b = a%10;
       // console.log(a)
        temp = temp*10 +b;
        //console.log(temp)
        a=Math.floor(a/10);
    }
    console.log(temp);
}
//revInt()


function palindromeString(){
    let str = 'foof';
    const revString = str.split('').reverse().join('');
    var result = str.localeCompare(revString);
    if(result<0){
        console.log('Not palindrome');
    }
    console.log('palindrome');
}
//palindromeString()


function findMiss(){
    let missArray = [];
    const array =[1,2,3,-2,0,10];

    let a = Math.min(...array);
    let b = Math.max(...array)
    //console.log(a);
    //console.log(b);
    const sorted = array.sort()
    for(let i=a; i<=b; i++){
        if(sorted.indexOf(i)<0){
            missArray.push(i);
        }
    }
    console.log(missArray);
}
//findMiss()


function fibonacci(){
    let count=8;
    let a=0;
    let b=1;
    let c
    console.log(a);
    console.log(b)

    while(count>0){
        c=a+b;
        a=b;
        b=c;
        console.log(c)
        count--
    }
}
//fibonacci()


// function genreateFib(){
//     let count = 10;
//     let a = 0;
//     let b = 1;
//     console.log(a);
//     console.log(b);
//     RecurrsiveFib(count)
// }
function RecurrsiveFib(co){
    
    if (co < 3) {
        return 1
    }
    return RecurrsiveFib(co - 1) + RecurrsiveFib(co - 2)
}
//document.write(RecurrsiveFib(10));
//console.log(RecurrsiveFib(10))

function EvenOrOdd(){
    arr = [1,2,3,4,5,6];
    let even=0;
    let odd=0;

    for(let i=0; i<arr.length; i++){
        if(arr[i]%2==0){
            console.log('even :',arr[i]);
        }
        console.log('odd :',arr[i]);
    }
}
//EvenOrOdd();

function findPrime(){
    let n = 100
    for(let i=3; i<=n; i++){
        if(i%i==0 && i%1==0){
            console.log(i,' is prime');
        }else{
            console.log(i,' is composite');
        }
    }
}
//findPrime()

function isPrime( n)
{
    if(n == 1 || n == 0) return false;
	for(var i = 2; i < n; i++)
	{
		if(n % i == 0) return false;
	}
	return true;
}
var N = 100;

for(var i = 1; i <= N; i++)
{
	if(isPrime(i)) {
		console.log( i );
	}
}
//isPrime(100)


function duplicate(){
    let array = [1,2,3,4,23,5,4,3,2,7,8,9,23,23,4,0];
    let result = [];
    let sorted = array.slice().sort();
    console.log(sorted);

    for(let i=0; i<array.length; i++){
        let min = Math.min(...sorted);
        
        if(sorted[i+1]==sorted[i]){
            result.push(sorted[i]);
        }
    }
    console.log(result)
}
//duplicate()

let str1="hello"
let str2="ellos"

function anagram(s, t) {
    if(s === t) return true
    if(s.length !== t.length) return false
    let count = {}

    for(let letter of s)
        count[letter] = (count[letter] || 0) + 1

    for(let letter of t) {
        if(!count[letter]) return false
        else --count[letter]
    }

   return true;
}
// console.log(anagram(str1,str2))



class Node{
    constructor(data, next=null){
        this.data = data;
        this.next = next
    }
}

class linkedList{
    constructor(){
        this.head = null;
        this.size= 0;
    }

    //insert first:----
    insertFirst(data){
        this.head = new Node(data, this.head);
        this.size++;

    }

    //insert last:----
    insertLast(data){
        let newNode = new Node(data)
        let current;

        //if empty make it head
        if(!this.head){
            this.head = newNode;

        }else{
            current = this.head;
            while(current.next){
                current = current.next;
            }
            current.next = newNode;
        }
        this.size++;
        return newNode;
    }

    //insert at index:----index:-(0,1,2,3,4,....)
    inserAtIndex(data, index) {

        let newNode = new Node(data)

        if(index>0 && index > this.size){
            console.log(`index:${index} is out of range`);
            return;
        }

        if(index < 0 ){
            return;
        }

        if(index === 0){

            if(!this.head){
                this.head = new Node(data, this.head);
                this.size++;
            }
            console.log("Head exists already try index > 0")
            return;
        }
        let current = this.head;
        let previous;
        let count=0
        while(count < index){
            previous = current;
            count++
            current = current.next;
        }  
        previous.next = newNode;
        newNode.next = current;
        this.size++
    }

    //Get data at index:----
    getElementAt(index) {
        let current = this.head;
        let count=0;
        while(current) {
            if(count == index){
                console.log(`Element at index ${index} is ${current.data}`);
            }
            count++;
            current = current.next;
        }
        return;
    }

    //Remove at index:----

    removeAt(index) {
        let current = this.head;
        let previous;
        let count = 0;

        if(index > 0 && index > this.size) {
            console.log("out of range");
            return;
        }

        if(index < 0) {
            return;
        }

        if(index == 0) {
            if(this.head){
                this.head = current.next;
                return;
            }
        }   
        
        if(index > 0) {
            while(count < index) {
                count++;
                previous = current;
                current = current.next;
            }
            previous.next = current.next;
        }
        this.size--;
    }

    //clear list
    clearList(){
        this.head = null;
        this.size = 0;
    }


    //printlist data:----
    printlist(){
        let current = this.head;
        let arr = [];
        while(current){
            arr.push(current.data)
            current = current.next
        }
        console.log(arr);
    }

    //print size
    returnSize(){
        return this.size;
    }
}

const l1 = new linkedList();
const l2 = new linkedList();

const intersection = l1.insertLast(4);
l1.insertLast(5);
l1.insertLast(6);
l1.insertLast(7);
l1.insertLast(8);
l1.insertLast(9);

l2.insertLast(11)
l2.insertLast(12)
const node = l2.insertLast(13)
node.next = intersection;
// l2.insertLast(13)
// l2.insertLast(6)
// l2.insertLast(7)
// l2.insertLast(8)
// l2.insertLast(9)

//////////////////////////////////////////////// Method 1 ////////////////////////////////////////////////
var getIntersectionNode = function(headA, headB) {
    let hA = l1.head;
    let hB = l2.head;
    while (hA !== hB) {
        hA = hA === null ? headB : hA.next;
        hB = hB === null ? headA : hB.next;
    };
    return hA;
};

// console.log(getIntersectionNode(l1.head, l2.head));

//////////////////////////////////////////////// method 2 ////////////////////////////////////////////////
let x = l1.head
let y = l2.head;

while(x != y){
    if(x === null){
        x = l2.head;
    }else{
        x = x.next;
    }
    
    if(y === null){
        y = l1.head;
    }else{
        y = y.next;
    }
    // x = x === null ? l2.head : x.next;
    // y = y === null ? l1.head : y.next
}
// console.log(x)

let array = [80, 60, 10, 50, 30, 100, 0, 50];
let target = 100;
const pairs = [];

function findPairs(array, target) {
    const complements = new Set();
  
    for (let i = 0; i < array.length; i++) {
      const complement = target - array[i];
      if (complements.has(complement)) {
        pairs.push([array[i], complement]);
      } else {
        complements.add(array[i]);
      }
    }
}
findPairs(array, target);

// console.log(pairs);

let str = ["eat","tea","tan","ate","nat","bat"];
let sortedStr = []
function anagramsGroup() {
    let map = new Map();
    for(let i = 0; i < str.length; i++) {
        sortedStr.push(str[i].split('').sort().join(''));  
        if (!map.has(sortedStr[i])) {
            map.set(sortedStr[i], [str[i]]);
        } else {
            map.get(sortedStr[i]).push(str[i]);
        }
    }
}
anagramsGroup(str);

// console.log([...map.values()]);  

let b = "Engineer";
let a = b.toLowerCase();
const hash = new Map();

for(let i = 0; i < a.length; i++) {
    if(!hash.has(a[i])) {
        hash.set(a[i], 1)
    } else {
        hash.set(a[i], hash.get(a[i])+1);
    }
}
let result = '';
for (const [char, frequency] of hash) {
    result += char + frequency;
  }
// console.log(result);
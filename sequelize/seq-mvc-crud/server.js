const express = require('express');
//const cors = require('cors');
const app = express();

const PORT = 8081;

//middlewares

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Routers

const product_router = require('./routes/productRouter.js');
app.use('/api/products', product_router)

const review_router = require('./routes/reviewRouter.js');
app.use('/api/reviews', review_router);

const customer_router = require('./routes/customerRouter.js')
app.use('/api/customers', customer_router)
// testing api

app.get('/', (req,res)=>{
    res.json({message: 'hello from get api'})
})

app.listen(PORT, ()=>{
    console.log(`running on port ${PORT}`);
})
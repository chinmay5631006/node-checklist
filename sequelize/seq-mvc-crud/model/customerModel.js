// const { DataTypes } = require("sequelize");
// const { sequelize } = require(".");

// const { customer } = require(".")

module.exports = (sequelize, DataTypes) => {
    
    const Customer = sequelize.define('customer',{
        name:{
            type: DataTypes.STRING,
            allowNull:false
        },
        email:{
            type: DataTypes.STRING,
        }
    },{
        timestamps: false
    })
    
    return Customer;
}

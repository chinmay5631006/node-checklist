const dbConfig = require('../config/dbconfig.js');
const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize(
    dbConfig.DB,
    dbConfig.USER,
    dbConfig.PASSWORD,
    {
        host: dbConfig.HOST,
        dialect: dbConfig.dialect,

        pool: {
            max: dbConfig.pool.max,
            min: dbConfig.pool.min,
            acquire: dbConfig.pool.acquire,
            idle: dbConfig.pool.idle
        }
    }
)

sequelize.authenticate()
    .then(()=>{
        console.log('db connected and authenticated')
    })
    .catch((e)=>{
        console.log(e);
    })

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Product = require('../model/productModel.js')(sequelize, DataTypes);
db.reviews = require('../model/reviewModel.js')(sequelize, DataTypes);
db.Customer = require('../model/customerModel.js')(sequelize, DataTypes);

////Associations :- hasMany and belongsTo/////

db.Product.hasMany(db.reviews)
db.reviews.belongsTo(db.Product)

let prod;
let rev;

db.Product.findOne({
    where: {title :'efgh'}
})
    .then((data) => {
        prod = data;
        return db.reviews.findAll();
    })
    .then((data) => {
        rev = data;
        return prod.addReview(rev)
    })
    .then((data) => {
        console.log(data);
        return db.Product.findOne({
            where: {}
        })
    })
    .catch((e)=>{
        console.log(e)
    })


////Associations :- hasMany and belongsTo////



db.sequelize.sync({ alter:true })
    .then(()=>{
        console.log('db synced');
    })
    .catch((e)=>{
        console.log(e);
    })

module.exports = db;

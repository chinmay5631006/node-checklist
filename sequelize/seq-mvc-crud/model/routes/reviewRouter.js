const reviewController = require('../controllers/reviewController.js');

const router = require('express').Router();

router.post('/addReview', reviewController.addReview);
router.get('/getReviews', reviewController.getAllReviews);
router.put('/updateReview/:id', reviewController.upadateReview);
router.delete('/deleteReview/:id', reviewController.deleteReview);

module.exports = router;

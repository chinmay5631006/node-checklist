const productController = require('../controllers/productController.js');

const router = require('express').Router();

router.post('/addProduct', productController.addProduct);
router.get('/AllProducts', productController.getAllProducts);
router.get('/Product/:id', productController.getProduct);
router.put('/update/:id', productController.updateProduct);
router.delete('/delete/:id', productController.deleteProduct);
router.get('/PublishedProduct', productController.getPublishedProduct);

module.exports = router;

const customerController = require('../controllers/customerController.js');

const router = require('express').Router();

router.post('/createCustomer', customerController.createCustomer);
router.get('/allCustomers', customerController.getAllCustomers);
router.get('/singleCustomer/:id', customerController.singleCustomer);
router.put('/updateCustomer/:id', customerController.updateCustomer);
router.delete('/deleteCustomer/:id', customerController.deleteCustomer);

module.exports = router;

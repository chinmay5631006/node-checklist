const { Product } = require('../model');

//create main model


//const Review = db.reviews;

// Main work

// 1. Create Prodcts

const addProduct = async (req,res) => {

    let info = {
        title: req.body.title,
        price: req.body.price,
        description: req.body.description,
        published: req.body.published
    }

    const product = await Product.create(info);
    res.status(200).send(product);
    console.log(product);
}

// 2. Get all products

const getAllProducts = async (req, res) => {
    const allProducts = await Product.findAll({
        attributes:[
            'title',
            'price',
            'description',
            'published'
        ]
    });
    res.status(200).send(allProducts);
}

// 3. Get single product

const getProduct = async (req, res) => {

    let id = req.params.id
    const singleProduct = await Product.findOne({where: {id: id}});
    res.status(200).send(singleProduct);
}


// 4. Update product

const updateProduct = async (req, res) => {

    let id = req.params.id;
    const { title, price, description, published } = req.body;
    const findProduct = await Product.findOne({where: {id: id}});
    if(!findProduct){
        return res.status(404).send('Not Found')
    }
    if(findProduct){
        const toupdate = Product.update({ title, price, description, published },{where: {id: id}});
        res.status(200).send('Products updated');
    }
}

// 5. Delete product

const deleteProduct = async (req, res) => {
    let id = req.params.id;
    await Product.destroy({where: {id: id}})
    res.status(200).send('Product Deleted');
}

// 6. get all published product

const getPublishedProduct = async (req, res) => {
    const publishedProduct = await Product.findAll({where: { published:true}});
    res.status(200).send(publishedProduct);
}

module.exports = {
    addProduct,
    getAllProducts,
    getProduct,
    updateProduct,
    deleteProduct,
    getPublishedProduct
}

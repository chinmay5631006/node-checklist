// const { Review } = require('../model');
const db = require('../model');

const Review = db.reviews

// 1. create review

const addReview = async (req,res) => {

    let info = {
        rating: req.body.rating,
        description: req.body.description
    }
    const review = await Review.create(info)
    res.status(200).send(review);
    console.log(review);
}

//2. get all reviews

const getAllReviews = async (req, res) => {
    const allReviews = await Review.findAll();
    res.status(200).send(allReviews);
}

// 3. update a review

const upadateReview = async (req, res) => {
    let id = req.params.id;
    const { rating, description } = req.body;
    const findReview = await Review.findOne({where: {id: id}});
    if(!findReview){
        return res.status(404).send('Not Found');
    }
    if(findReview){
        const toUpdate = Review.update({rating, description},{where: {id: id}});
        res.status(200).send('Reviews updated');
    }
}

// 4. Delete a review

const deleteReview = async (req, res) => {
    let id = req.params.id;
    await Review.destroy({where: {id: id}});
    res.status(200).send("review deleted")
}

module.exports = {
    addReview,
    getAllReviews,
    upadateReview,   
}
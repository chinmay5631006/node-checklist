const { Customer } = require('../model');

// 1. create

const createCustomer = async (req, res) => {
    
    let info = {
        name: req.body.name,
        email: req.body.email
    }

    const addCustomer = await Customer.create(info);
    res.status(200).send(addCustomer);
}

// 2. get all customers

const getAllCustomers = async (req, res) => {
    const allCustomers = await Customer.findAll();
    res.status(200).send(allCustomers);
}

// 3. get one customer

const singleCustomer = async (req, res) => {
    let id = req.params.id;
    const aCustomer = await Customer.findOne({where: {id: id}});
    res.status(200).send(aCustomer);
}

// 4. Update Customer

const updateCustomer = async (req, res) => {
    let id = req.params.id;
    const {name, email} = req.body;
    const findCustomer= await Customer.findOne({where: {id: id}});
    if(!findCustomer){
       return res.status(400).send('not found')
    }
    if(findCustomer){
        const toupdate = await Customer.update({name: req.body.name, email: req.body.email},{where: {id: id}})
        res.status(200).send('Customer updated')
    }
}

// 5. Delete Customer

const deleteCustomer = async (req, res) => {
    let id = req.params.id;
    await Customer.destroy({where: {id: id}});
    res.status(200).send('customer deleted');
}

module.exports = {
    createCustomer,
    getAllCustomers,
    singleCustomer,
    updateCustomer,
    deleteCustomer
}

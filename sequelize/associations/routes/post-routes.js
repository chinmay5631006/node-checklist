const express = require('express')
const router = express.Router();
const db = require('../models');

router.post('/new', (req, res) => {
    db.Post.create({
        text: req.body.text,
        UserId: req.body.UserId
    })
    .then((newPost) => { res.send(newPost) })
    .catch((e) => { console.log(e) })
})

router.get('/all', (req, res) => {
    db.Post.findAll({
        include: [db.User]
    })
        .then((allPosts) => { res.send(allPosts) })
        .catch((e) => { console.log(e) })
})

router.get('/find/:id', (req, res) => {
    db.Post.findOne({
        where: {UserId: req.params.id},
        include: [db.User]
    })
    .then((Post) => { res.send(Post) })
    .catch((e) => { console.log(e) })
})

module.exports = router;

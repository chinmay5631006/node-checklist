const express = require('express')
const router = express.Router();
const db = require('../models');

router.post('/new', (req, res) => {
    db.Profile.create({
        name: req.body.name,
        UserId: req.body.UserId
    })
    .then((newProfile) => { res.send(newProfile) })
    .catch((e) => { console.log(e) })
})

router.get('/all', (req, res) => {
    db.Profile.findAll({
        include: [db.User]
    })
        .then((allProfiles) => { res.send(allProfiles) })
        .catch((e) => { console.log(e) })
})

router.get('/find/:id', (req, res) => {
    db.Profile.findOne({
        where: {UserId: req.params.id},
        include: [db.User]
    })
    .then((Profile) => { res.send(Profile) })
    .catch((e) => { console.log(e) })
})

module.exports = router;

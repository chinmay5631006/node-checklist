const express = require('express');
const app = express();
const db = require('./models');
const PORT = 3000;

//middlewares
app.use(express.urlencoded({ extended:true }));
app.use(express.json());

//routes

const userRoutes = require('./routes/user-routes.js');
app.use('/api/users', userRoutes);

const profileRoutes = require('./routes/profile-routes.js');
app.use('/api/profiles', profileRoutes);

const postRoutes = require('./routes/post-routes.js');
app.use('/api/posts', postRoutes);

// test route:-

app.get('/', (req, res) => { res.send("Express Running...") })

db.sequelize.sync()
    .then(()=>{
        app.listen( PORT, ()=>{ console.log(`App running on Port 3000...`) })
    })
    .catch((e)=>{ console.log(e) })
const Sequelize = require('sequelize');
const{ DataTypes, Op } = require('sequelize')

const connection = new Sequelize('sequelize-video', 'root', 'abcd@123', {
    dialect: 'mysql'
})

async function auth(){
    try{
        await connection.authenticate();
        console.log("connection with DB successfull")
    }catch(e){
        console.log(e);
    }
}
auth();

const Students = connection.define('Student',{
    student_id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    name:{
        type:DataTypes.STRING,
        allowNull: false,
        validate:{
            len:[4,20]
        }
    },
    favorite_class:{
        type:DataTypes.STRING,
        default:'Computer Science',
    },
    scchool_year:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    subscribed_to_wittcode:{
        type:DataTypes.TINYINT,
        default:true
    }
},
{
    freezeTable: true,
    timestamps: false
})


Students.sync({alter:true})
    // .then(()=>{
    //     return Students.bulkCreate([         //already done
    //         {
    //             name:'Virendra',
    //             favorite_class:'Cloud',
    //             scchool_year:'4',
    //             subscribed_to_wittcode:'1'
    //         },
    //         {
    //             name:'Satyendra',
    //             favorite_class:'Proect_management',
    //             scchool_year:'5',
    //             subscribed_to_wittcode:'0'
    //         },
    //         {
    //             name:'Rohan',
    //             favorite_class:'python',
    //             scchool_year:'4',
    //             subscribed_to_wittcode:'1'
    //         },
    //         {
    //             name:'Chaitanya',
    //             favorite_class:'React',
    //             scchool_year:'4',
    //             subscribed_to_wittcode:'0'
    //         }
    //     ],
    //     {validate:true}
    //     )
    //         .then((data)=>{
    //             data.forEach(element => {
    //                 console.log(element.toJSON()); 
    //             });
    //         })
    //         .catch((e)=>{
    //             console.log(e);
    //         })
    // })
    .then(()=>{
        return Students.findAll({where:{
            [Op.or]:{favorite_class:'Computer Science', subscribed_to_wittcode:'1'}
        }})
    })
    .then((data)=>{
        data.forEach(element =>{
            console.log(element.toJSON());
        })
    })
    .then(()=>{
        return Students.findAll({

            attributes:['scchool_year', [connection.fn('COUNT', connection.col('name')), 'num_students']],
            group: 'scchool_year'
        })
    })
    .then((data)=>{
        data.forEach(elements =>{
            console.log(elements.toJSON());
        })
    })
    .then(()=>{
        console.log("done");
    })
    .catch((e)=>{
        console.log(e);
    })

// Students.sync({alter:true})
//     .then(()=>{
//         return Students.create({
//             name:'Amruta',
//             favorite_class:'Algorithms',
//             scchool_year:'4',
//             subscribed_to_wittcode:'0'
//         })
//     })
//     .then((data)=>{
//         console.log(data.toJSON())
//     })
//     .catch((e)=>{console.log(e)})
async function displayCommits(){
    try{
        const user = await getUser(1);
        const repos = await getRepositories(user.gitHubUsername)
        const commits = await getCommits(repos);
        console.log(repos)
        console.log(commits)
    }catch(err){
        console.log('Error', err.message);
    }    
}
displayCommits()

console.log('After');

function getUser(id) {
   return new Promise((resolve,reject)=>{
       setTimeout(() => {
           console.log('Reading a user from a database...');
           resolve({ id: id, gitHubUsername: 'mosh' });
           //reject('Id not found')
         }, 2000);
   });
}

function getRepositories(username) {
   return new Promise((resolve,reject)=>{
       setTimeout(() => {
           console.log('Calling GitHub API...');
           //resolve(`['repo1','repo2']`);
           reject(new Error('Repo not found'))
         }, 2000);
   });
}

function getCommits(repos) {
   return new Promise((resolve,reject)=>{
       setTimeout(() => {
           console.log('Calling GitHub API...');
           resolve(`[commit]`);
         }, 2000);
   });
}
//named function

function evenOdd(){
    let a = 22;
    if(a%2===0){
        console.log('Even number');
    }else{    
        console.log('Odd number');
    }    
}
evenOdd()

//Anonymous Function
let n = 22
let result = function(n){
    if(n%2===0){
        return 'Number is even'
    }
    return 'Number is odd';
}
console.log(result(n));

//Immediately invoked function
(function () {
    console.log("hi");
})();
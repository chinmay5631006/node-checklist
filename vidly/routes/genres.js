const express = require('express');
const router = express.Router();

const genres = [
    { id: 1, name: 'Action' },  
    { id: 2, name: 'Horror' },  
    { id: 3, name: 'Romance' },  
  ];

router.get('/', (req,res)=>{
    res.send(genres);
})

router.post('/',(req,res)=>{
    const validationResult = valid(req.body);
    if(validationResult.error){
        res.status(400).send(validationResult.error.details[0].message);
        return;
    };    
        const genre={
            id : genres.length + 1,
            name:req.body.name
        };
        genres.push(genre);
        res.send(genres);
    
});

router.put('/:id',(req,res)=>{
    const genreFound = genres.find(c => c.id===Number(req.params.id))
    console.log(genreFound)
    if(!genreFound) return res.status(404).send('provide valid genre')

    const validationResult = valid({name: req.body.name});
    if(validationResult.error){
        res.status(400).send(validationResult.error.details[0].message);
        return;
    }   
    
    genreFound.name = req.body.name;
    res.send(genreFound);
});

router.delete('/:id',(req,res)=>{
    const genreFound = genres.find(c => c.id===parseInt(req.params.id))
    if(!genreFound) return res.status(404).send('provide valid genre')

    const index = genres.indexOf(genreFound);
    genres.splice(index, 1);

    res.send(genreFound);
})

function valid(body){
    const schema = Joi.object({
        name: Joi.string().min(3).required()
      });
    
      return schema.validate(body);
}

module.exports = router;

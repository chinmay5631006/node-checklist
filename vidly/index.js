const express = require('express');
const app = express();
const PORT = 8000;
const Joi = require('joi');
const genres = require('./routes/genres')

app.use(express.json());                                            //middleware
app.use('/vidly.com/api/genres', genres);
app.use((req,res,next)=>{
    
});
  
app.get('/vidly.com', (req,res)=>{
    res.send('Welcome to Vidly');
})

    

app.listen(PORT, ()=>console.log(`Server running on ${PORT}`));

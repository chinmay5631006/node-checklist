const connection = require('./connection');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const PORT = 4010

app.use(bodyParser.json());

//read
app.get('/employee',(req,res)=>{
    connection.query('SELECT * FROM employee', (err,rows)=>{
        if(err){
            console.log(err);
        }else{
            //console.log(rows);
            res.send(rows);
        }
    })
});

app.get('/employee/:emp_id', (req,res)=>{
    connection.query('SELECT * FROM employee WHERE emp_id=?',[req.params.emp_id],(err,rows)=>{
        if(err){
            console.log(err);
        }else{
            res.send(rows);
        }    
    });
})

//delete
app.delete('/employee/:emp_id', (req,res)=>{
    connection.query('DELETE FROM employee WHERE emp_id=?',[req.params.emp_id],(err,rows)=>{
        if(err){
            console.log(err);
        }else{
           res.send(rows);
        }
    })
})

//create
app.post('/employee', (req,res)=>{
    let emp = req.body;
    let empData = [emp.name, emp.salary];
    connection.query('INSERT INTO employee(name,salary) VALUES(?)',[empData],(err,rows)=>{
        if(err){
            console.log(err);
        }else{
           res.send(rows);
        }
    });
});

//update 1)put:- to change all the details  2)patch:- to change some fields
app.patch('/employee', (req,res)=>{
    let emp = req.body;
    connection.query('UPDATE employee SET ? WHERE emp_id='+emp.emp_id,[emp],(err,rows)=>{
        if(err){
            console.log(err);
        }else{
           res.send(rows);
        }
    });
});

app.put('/employee', (req,res)=>{
    let emp = req.body;
    connection.query('UPDATE employee SET ? WHERE emp_id='+emp.emp_id,[emp],(err,rows)=>{
        if(err){
            console.log(err);
        }else{
          if(rows.affectedRows==0){
            let empData = [emp.name, emp.salary];
            connection.query('INSERT INTO employee(name,salary) VALUES(?)',[empData],(err,rows)=>{
                if(err){
                    console.log(err);
                }else{
                   res.send(rows);
                }
            });
          }
        }
    });
});


app.listen(PORT, ()=>console.log(`Express running on ${PORT}`));

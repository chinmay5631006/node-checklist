const mysql = require('mysql2');

var mysqlConnection = mysql.createConnection({
    host:'127.0.0.1',
    user:'root',
    password:'abcd@123',
    database:'employee_db'
});

mysqlConnection.connect((err)=>{
    if(err){
        console.log("Error in db_connection"+JSON.stringify(err,undefined,2));
    }else{
        console.log("db connected successfully");
    }
})
module.exports = mysqlConnection;